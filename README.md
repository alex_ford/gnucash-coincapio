# gnucash-coincapio

**Author**: Alex Ford (arf4188@gmail.com)

**License**: MIT License (see LICENSE file for full text)

This small script project is intended to help in the downloading and importing of cryptocurrency data (like Bitcoin, Ethereum, etc.) into GnuCash.

**Maturity**: Level 0 / 4 (Not Ready) :no_entry:

This project is not ready to be used by end-users or developers. Please come back later, or contact the author if you'd like to find out how to help contribute. http://www.alexrichardford.com

## Getting Started (Windows)

1. Clone this repo to your local computer: `git clone https://gitlab.com/alex_ford/gnucash-coincapio.git`
1. Install NodeJS by going to their website and grabbing the installer for the latest version.
1. Use npm to fetch dependencies: `npm install`
1. Use npm to run the app: `node app.js`

## GnuCash Setup

In order to make use of this project, you must already be a user of GnuCash to track your finances and investments. You must also have it setup to treat cryptocurrencies as stocks/funds.

If, for example, you have bitcoins up in the Coinbase Exchange, then in GnuCash, you should create a new parent account called "Coinbase", then another account of the Stock type, called "BTC Wallet" using the BTC security. If you did not already create the BTC security, it would be a good idea to do so as well. Increase the decimal places to 1/10000000. Do NOT go beyond that level of precision. GnuCash cannot handle it! This is a known bug/limitation of the software.

## Developer Notes

This interfaces directly with the Coincap.io API which is a simple JSON API accessed using parameterized endpoint URLs.

TODO still trying to figure out exactly how I want to interface with GnuCash. There is a Python API, a C API, and perhaps other ways? Maybe even just a CLI... all I need to do is push the price data I've pulled down from the Coincap.io API into the GnuCash database. Also, can I support both XML and the SQL stores?
