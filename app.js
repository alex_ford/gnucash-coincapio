//
// Main entry point of the gnucash-coincapio script project.
//
// Author: Alex Ford (arf4188@gmail.com)
//

const http = require('http')

var endpoint_coins = 'http://www.coincap.io/coins';
var endpoint_hist1day = 'http://www.coincap.io/history/1day/';

console.log('hello world');

function httpRequest(url, callback) {
    http.get(url, function(response) {
        var body = '';

        response.on('data', function(chunk) {
            body += chunk;
        });

        response.on('end', function() {
            var coincapResponse = JSON.parse(body);
            //console.log("Got a response: ", coincapResponse);
            callback(coincapResponse);
        });
    }).on('error', function(e) {
        console.log("ERROR: ", e);
    });
}

// Function for getting the price data for exactly
// one cryptocurrency coin.
function requestCryptoPrice(coin, callback) {
    httpRequest(endpoint_hist1day + coin, function(response) {
        callback(response.price[response.price.length-1]);
    });
}

// httpRequest(endpoint_coins, function(resp) {
//     console.log(resp);
// });

requestCryptoPrice('BTC', function(response) {
    console.log(response);
});
